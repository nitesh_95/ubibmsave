package com.subk.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.subk.dao.LoanOriginationRepository;
import com.subk.model.Loanorigination;
import com.subk.util.Base64Coder;
import com.subk.util.HTTPSRequest;
import com.subk.util.XMLParser;

@Service
public class UserService {


	@Autowired
	LoanOriginationRepository repository;

	public List<Loanorigination> getAll() {
		List<Loanorigination> userList = repository.findAll();
		
		if (userList.size() > 0) {
			return userList;
		} else {
			return new ArrayList<Loanorigination>();
		}
	}
	



	public Loanorigination getUserById(Long id) throws RecordNotFoundException {
		Optional<Loanorigination> user = repository.findById(id);
		if (user.isPresent()) {
			return user.get();
		} else {
			throw new RecordNotFoundException("No employee record exist for given id");
		}
	}

	public boolean createOrUpdate(Loanorigination entity) throws RecordNotFoundException {
		Optional<Loanorigination> users = repository.findById(entity.getApplicationid());

		if (users.isPresent()) {
			System.out.println("is present");
			System.out.println("Loan Origination   " + entity.toString());
			int count = repository.updateLoanSanctionedDetails(entity.getApplicationid(),
					entity.getLoanamountsactioned(), entity.getNoofinstalments(), entity.getSavingaccnumber(),
					entity.getBankcustid(), entity.getInstalmentamount(), entity.getBankremarks(),
					entity.getBankverificationstatus(),entity.getSolid(),entity.getInterestrate());
//			System.out.println("apppidddddddddddddd"+entity.getApplicationid());
			if (count > 0) {
				HashMap map = approveLoanStatus(entity.getSolid(), String.valueOf(entity.getApplicationid()),
						entity.getBankremarks(), entity.getBankverificationstatus(), entity.getLoanamountsactioned(),
						entity.getInterestrate(), entity.getNoofinstalments(), entity.getInstalmentamount(), entity.getSavingaccnumber(),
						entity.getBankcustid());
				if (map.get("Status") == "00") {
					return true;
				} else {
					return false;
				}

			} else {
				return false;
			}
		} else {
			return false;
		}



	}

	public HashMap approveLoanStatus(String solId, String applNo, String remarks, String status, String loanAmt,
			String rate, String term, String emiAmt, String custsavingAcno, String custBankCustID) {
		InputStream ins = null;
		HashMap map = null;
		try {
			String xmlString = "<Loan_Origination>" + "<LoginId>" + Base64Coder.encodeString(solId) + "</LoginId>"
					+ "<Designation>" + Base64Coder.encodeString("BM") + "</Designation>" + "<ApplicationId>"
					+ Base64Coder.encodeString(applNo) + "</ApplicationId>" + "<VerificationStatus>"
					+ Base64Coder.encodeString(status) + "</VerificationStatus>" + "<Remarks>"
					+ Base64Coder.encodeString(remarks) + "</Remarks>" + "<ApprovedLoanAmount>"
					+ Base64Coder.encodeString(loanAmt) + "</ApprovedLoanAmount>" + "<InterestRate>"
					+ Base64Coder.encodeString(rate) + "</InterestRate>" + "<NoOfTerms>"
					+ Base64Coder.encodeString(term) + "</NoOfTerms>" + "<EMIAmount>" + Base64Coder.encodeString(emiAmt)
					+ "</EMIAmount>";
			if (custsavingAcno != null && !custsavingAcno.equals("") && custsavingAcno != "") {
				xmlString += "<SavingAccountNo>" + Base64Coder.encodeString(custsavingAcno) + "</SavingAccountNo>";
			} else {
				xmlString += "<SavingAccountNo></SavingAccountNo>";
			}
			if (custBankCustID != null && !custBankCustID.equals("") && custBankCustID != "") {
				xmlString += "<BankCustomerId>" + Base64Coder.encodeString(custBankCustID) + "</BankCustomerId>";
			} else {
				xmlString += "<BankCustomerId></BankCustomerId>";
			}
			xmlString += "</Loan_Origination>";
			HTTPSRequest httpReq = new HTTPSRequest();
//            HTTPRequest httpReq = new HTTPRequest();
			String url = "https://172.18.1.140:8443/UBIManagerSwitch/" + "updateVerificationStatus";
			System.out.println("url:::::::::::::::::::::::::::::::::::::   " + url);
			ins = httpReq.hitHttpsXMLData("https://172.18.1.140:8443/UBIManagerSwitch/" + "updateVerificationStatus",
					xmlString);
//            System.out.println("RESPONSE: "+InputStreamToString.getString(ins));
			XMLParser parser = new XMLParser(ins);
			map = parser.getMap();
//            log.info("HASHMAP : " + map);
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (ins != null) {
				try {
					ins.close();
				} catch (Exception e) {
				}
			}
		}
		return map;
	}
}