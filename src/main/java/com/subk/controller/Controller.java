package com.subk.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.subk.model.Loanorigination;
import com.subk.service.RecordNotFoundException;
import com.subk.service.UserService;
import com.subk.util.Response;

@RestController
@CrossOrigin
public class Controller {
	
	Logger log = LoggerFactory.getLogger(Controller.class);

	@Autowired
	private UserService service;

	@GetMapping
	public ResponseEntity<List<Loanorigination>> getAllUsers() {
		List<Loanorigination> list = service.getAll();
		return new ResponseEntity<List<Loanorigination>>(list, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Loanorigination> getEmployeeById(@PathVariable("id") Long id) throws RecordNotFoundException {
		Loanorigination user = service.getUserById(id);
		return new ResponseEntity<Loanorigination>(user, new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping("/users/submit")
	public Response createOrUpdate(@RequestBody Loanorigination userData) throws RecordNotFoundException {
		Response resp = new Response();
		log.info("request:::::::::::::" + userData.toString());
		boolean updated = service.createOrUpdate(userData);
		if (updated) {
			resp.setStatus(HttpStatus.OK.toString());
			resp.setRemarks("Success");

		} else {
			resp.setStatus(HttpStatus.OK.toString());
			resp.setRemarks("Failed to Insert/updated the customer data");
		}
		return resp;
	}
	
}