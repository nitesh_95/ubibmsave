package com.subk.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.subk.model.Loanorigination;;

public interface LoanOriginationRepository extends JpaRepository<Loanorigination, Long>{
@Transactional
@Modifying
@Query("UPDATE Loanorigination SET Loanamountsactioned = :Loanamountsactioned,Bankverificationstatus = :Bankverificationstatus, Noofinstalments = :Noofinstalments, Savingaccnumber = :Savingaccnumber, Bankcustid = :Bankcustid, Instalmentamount = :Instalmentamount, Bankremarks =  :Bankremarks, Interestrate = :Interestrate, Solid = :Solid WHERE id = :id")
int updateLoanSanctionedDetails(long id , String Loanamountsactioned,String Noofinstalments,String Savingaccnumber
		, String Bankcustid	,String Instalmentamount , String Bankremarks ,String Bankverificationstatus,String Solid,String Interestrate);
}
