package com.subk.util;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.security.Security;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
public class HTTPSRequest {
	
	public InputStream hitHttps(String urlString) throws Exception {
        System.setProperty("java.protocol.handler.pkgs", "javax.net.ssl");
        java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {

                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public javax.net.ssl.X509KeyManager getX509KeyManager() {
                    return null;
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                    System.out.println("certs1" + certs);
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                }
            }};
        Security.setProperty("ssl.SocketFactory.provider", "org.ablogic.search.AllTrustSSLSocketFactory");
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        URL url = new URL(urlString);
        HostnameVerifier hv = new HostnameVerifier() {

            public boolean verify(String urlHostName, SSLSession session) {
                System.out.println("Warning: URL Host: " + urlHostName + " vs. " + session.getPeerHost());
                return true;
            }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(hv);
        HttpsURLConnection conn = (javax.net.ssl.HttpsURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setRequestMethod("POST");
        conn.setUseCaches(false);
        String userName = "test4";
        String password = "test";
        conn.setRequestProperty("Authorization", "Basic " + Secure.encodeString(userName + ":" + password));
        conn.setRequestProperty("Content-length", "" + urlString.length());
        conn.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
        //System.out.println("url is--------------------" + urlString);
        conn.connect();
        InputStream is = conn.getInputStream();
        return is;
    }

    /**
     * 
     * @param urlString
     * @param xmlDataString
     * @return
     * @throws Exception 
     */
    public InputStream hitHttpsXMLData(String urlString, String xmlDataString) throws Exception {
        System.setProperty("java.protocol.handler.pkgs", "javax.net.ssl");
        java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
        TrustManager[] trustAllCerts = new TrustManager[]{
            new X509TrustManager() {

                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public javax.net.ssl.X509KeyManager getX509KeyManager() {
                    return null;
                }

                public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                    System.out.println("certs1" + certs);
                }

                public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {
                }
            }};
        Security.setProperty("ssl.SocketFactory.provider", "org.ablogic.search.AllTrustSSLSocketFactory");
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        URL url = new URL(urlString);

        HostnameVerifier hv = new HostnameVerifier() {

            public boolean verify(String urlHostName, SSLSession session) {
                System.out.println("Warning: URL Host: " + urlHostName + " vs. " + session.getPeerHost());
                return true;
            }
        };
        HttpsURLConnection.setDefaultHostnameVerifier(hv);
        HttpsURLConnection conn = (javax.net.ssl.HttpsURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setRequestMethod("POST");
        conn.setUseCaches(false);
        String userName = "test4";
        String password = "test";
        conn.setRequestProperty("Authorization", "Basic " + Secure.encodeString(userName + ":" + password));
        conn.setRequestProperty("Content-length", "" + urlString.length());
        conn.setRequestProperty("Content-type", "text/xml");
        //System.out.println("url is--------------------" + urlString);
        OutputStream out = conn.getOutputStream();
        out.write(xmlDataString.toString().getBytes());
        conn.connect();
        InputStream is = conn.getInputStream();
        return is;
    }

}
