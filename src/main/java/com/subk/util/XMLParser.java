package com.subk.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
public class XMLParser {
	
	 private final InputStream ins;
	    public XMLParser(InputStream ins) {
	        System.out.println("Parser has been initilized.");
	        this.ins = ins;
	    }
	    /**
	     * 
	     * @param ins
	     * @return 
	     */
	    public HashMap parseByDom() {
	        HashMap hashMap = new HashMap();
	        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        try {
	            //Using factory get an instance of document builder
	            DocumentBuilder db = dbf.newDocumentBuilder();
	            //parse using builder to get DOM representation of the XML file
	            Document doc = db.parse(ins);
	            Element element = doc.getDocumentElement();
	            NodeList responseNodes = element.getChildNodes();
	            for (int i = 0; i < responseNodes.getLength(); i++) {
	                Node node = responseNodes.item(i);
	                if (node.getNodeType() == 1) {
	                    String nodename = node.getNodeName();
	                    String content = node.getTextContent();
	                    hashMap.put(nodename, content);
	                }
	            }
	        } catch (Exception exception) {
	            System.out.println("Exception in Parsing the XML Data in XMLParser parseByDom method : " + exception);
	            hashMap.put("STATUS", "300");
	            hashMap.put("REASON", "Parsing Error : Improper Response From Server");
	            System.out.println(hashMap);
	            return hashMap;
	        }
	        System.out.println(hashMap);
	        return hashMap;
	    }
	    public HashMap<String, String> getMap() throws SAXException, ParserConfigurationException, IOException {
	       System.out.println("inside parse by dom");
	        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        HashMap ht = new HashMap();
	        String ipConfig= "";
//	        Using factory get an instance of document builder
	        DocumentBuilder db = dbf.newDocumentBuilder();
//	        parse using builder to get DOM representation of the XML file
	        Document doc = db.parse(ins);
	        Element element = doc.getDocumentElement();
	        NodeList responseNodes = element.getChildNodes();
	        for (int i = 0; i < responseNodes.getLength(); i++) {
	            Node node = responseNodes.item(i);
	            if (node.getNodeType() == 1) {
	                String nodename = node.getNodeName();
	                if (nodename.equals("AccessIP")) {
	                    ipConfig =ipConfig+ node.getAttributes().getNamedItem("ServRef").getNodeValue()+ "!" + node.getAttributes().getNamedItem("FP_IP").getNodeValue() + "!"+ node.getAttributes().getNamedItem("Mis_IP").getNodeValue() + "!" + node.getAttributes().getNamedItem("Trans_IP").getNodeValue()+"#";
	                    ht.put("AccessIP", ipConfig);
	                }else{
	                    String content = node.getTextContent();
	                    System.out.println(nodename + ":" + content);
	                    ht.put(nodename, content);
	                }
	            }
	        }
	        System.out.println("AccessIP :" + ipConfig);
	        System.out.println("parseByDom is finished"+ht);
	        return ht;
	    }

	    /**
	     * 
	     * @param xml_hashMap
	     * @return
	     * @throws Exception 
	     */
	    public String xmlresp(HashMap xml_hashMap) throws Exception {
	        StringBuilder stringbuilder = null;
	        try {
	            stringbuilder = new StringBuilder();
	            stringbuilder.append("<SUBK_GATEWAY>\n");
	            Set set = xml_hashMap.keySet();
	            Iterator iterator = set.iterator();
	            while (iterator.hasNext()) {
	                String key = iterator.next().toString();
	                stringbuilder.append("<").append(key).append(">").append(xml_hashMap.get(key)).append("</").append(key).append(">\n");
	            }
	            stringbuilder.append("</SUBK_GATEWAY>");
	            System.out.println("THE XML REPONSE :\n" + stringbuilder.toString());
	        } catch (Exception exception) {
	            System.out.println("Exception in xmlresp method of XMLParser : " + exception);
	        }
	        return stringbuilder.toString();
	    }

}
