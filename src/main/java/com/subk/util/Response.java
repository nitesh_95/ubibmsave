package com.subk.util;

import org.springframework.beans.factory.annotation.Value;

public class Response {

	private String status;
	private String remarks;
	@Value("${MCLRL}")
	private String mclr;
	@Value("${MCLRL_MALE}")
	private String mclrMale;
	@Value("${MCLRL_FEMALE}")
	private String mclrmalefemale;
	
	
	
	public String getMclr() {
		return mclr;
	}
	public void setMclr(String mclr) {
		this.mclr = mclr;
	}
	public String getMclrMale() {
		return mclrMale;
	}
	public void setMclrMale(String mclrMale) {
		this.mclrMale = mclrMale;
	}
	public String getMclrmalefemale() {
		return mclrmalefemale;
	}
	public void setMclrmalefemale(String mclrmalefemale) {
		this.mclrmalefemale = mclrmalefemale;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	@Override
	public String toString() {
		return "Response [status=" + status + ", remarks=" + remarks + ", mclr=" + mclr + ", mclrMale=" + mclrMale
				+ ", mclrmalefemale=" + mclrmalefemale + "]";
	}
}