package com.subk.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "loanorigination")
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
public class Loanorigination {
	@Id
	@Column(name = "Applicationid")
	private Long applicationid;
	@Column(name = "loanamountsactioned")
	private String loanamountsactioned;
	@Column(name = "noofinstalments")
	private String noofinstalments;
	@Column(name = "Instalmentamount")
	private String instalmentamount;
	@Column(name = "Savingaccnumber")
	private String savingaccnumber;
	@Column(name = "Bankcustid")
	private String bankcustid;
	@Column(name = "Bankremarks")
	private String bankremarks;
	@Column(name = "Bankverificationstatus")
	private String bankverificationstatus;
    @Column(name="Interestrate")
    private String interestrate;
    @Column(name="Solid")
    private String solid;
    
    
  
	
	public Long getApplicationid() {
		return applicationid;
	}

	public void setApplicationid(Long applicationid) {
		this.applicationid = applicationid;
	}

	public String getLoanamountsactioned() {
		return loanamountsactioned;
	}

	public void setLoanamountsactioned(String loanamountsactioned) {
		this.loanamountsactioned = loanamountsactioned;
	}

	public String getNoofinstalments() {
		return noofinstalments;
	}

	public void setNoofinstalments(String noofinstalments) {
		this.noofinstalments = noofinstalments;
	}

	public String getInstalmentamount() {
		return instalmentamount;
	}

	public void setInstalmentamount(String instalmentamount) {
		this.instalmentamount = instalmentamount;
	}

	public String getSavingaccnumber() {
		return savingaccnumber;
	}

	public void setSavingaccnumber(String savingaccnumber) {
		this.savingaccnumber = savingaccnumber;
	}

	public String getBankcustid() {
		return bankcustid;
	}

	public void setBankcustid(String bankcustid) {
		this.bankcustid = bankcustid;
	}

	public String getBankremarks() {
		return bankremarks;
	}

	public void setBankremarks(String bankremarks) {
		this.bankremarks = bankremarks;
	}

	public String getBankverificationstatus() {
		return bankverificationstatus;
	}

	public void setBankverificationstatus(String bankverificationstatus) {
		this.bankverificationstatus = bankverificationstatus;
	}
	

	public String getInterestrate() {
		return interestrate;
	}

	public void setInterestrate(String interestrate) {
		this.interestrate = interestrate;
	}

	public String getSolid() {
		return solid;
	}

	public void setSolid(String solid) {
		this.solid = solid;
	}

	@Override
	public String toString() {
		return "Loanorigination [applicationid=" + applicationid + ", loanamountsactioned=" + loanamountsactioned
				+ ", noofinstalments=" + noofinstalments + ", instalmentamount=" + instalmentamount
				+ ", savingaccnumber=" + savingaccnumber + ", bankcustid=" + bankcustid + ", bankremarks=" + bankremarks
				+ ", bankverificationstatus=" + bankverificationstatus + ", interestrate=" + interestrate + ", solid="
				+ solid + "]";
	}

}